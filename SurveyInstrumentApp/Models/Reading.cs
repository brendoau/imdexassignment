﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SurveyInstrumentApp.Models
{
    public class Reading
    {
        [Display(Name = "Reading ID")]
        public int ReadingID { get; set; }
        [Display(Name = "Hole ID")]
        public int HoleID { get; set; }

        public decimal Depth { get; set; }         public decimal Dip { get; set; }         public decimal Azimuth { get; set; }         public Boolean Trustworthy { get; set; }         public Boolean Incorrect { get; set; }

        public Hole Hole { get; set; }
    }
}