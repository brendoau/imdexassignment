﻿using SurveyInstrumentApp.Models;
using System;
using System.Linq;

namespace SurveyInstrumentApp.Data
{
    public static class SeedData
    {
        public static void Initialize(SurveyInstrumentAppContext context)
        {
            context.Database.EnsureCreated();

            // Look for any holes.
            if (context.Holes.Any())
            {
                return;   // DB has been seeded
            }

            //seed holes
            var holes = new Hole[3];
            for (int i = 0; i < 3; i++) {
                holes[i] = new Hole { Lat = 10.1M + i, Lon = 10.1M + i, Dip = 10.1M + i, Azimuth = 10.1M + i};
            } 

            foreach (Hole h in holes)
            {
                context.Holes.Add(h);
            }
            context.SaveChanges();

            //seed readings
            var readings = new Reading[300];
            var counter = 0;
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    readings[counter] = new Reading { HoleID = i, Depth = 101.1M - j, Dip = 20.1M + (j/10), Azimuth = 10.1M + (j/10), Trustworthy = true, Incorrect = false };
                    counter++;
                }
            }

            foreach (Reading r in readings)
            {
                context.Readings.Add(r);
            }
            context.SaveChanges();
        }
    }
}