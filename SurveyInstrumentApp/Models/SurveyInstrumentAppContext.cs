﻿using System;
using Microsoft.EntityFrameworkCore;

namespace SurveyInstrumentApp.Models
{
    public class SurveyInstrumentAppContext : DbContext
    {
        public SurveyInstrumentAppContext(DbContextOptions<SurveyInstrumentAppContext> options) 
            : base(options)
        {
        }

        public DbSet<Hole> Holes { get; set; }
        public DbSet<Reading> Readings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hole>().ToTable("Hole");
            modelBuilder.Entity<Reading>().ToTable("Reading");
        }
    }
}