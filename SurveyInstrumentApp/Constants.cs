﻿namespace SurveyInstrumentApp
{
    static class Constants
    {
        public const int DipThreshold = 3; //in degrees
        public const int AzimuthThreshold = 5; //in degrees
    }
}