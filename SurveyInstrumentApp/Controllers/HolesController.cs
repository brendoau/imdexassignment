using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SurveyInstrumentApp.Models;

namespace SurveyInstrumentApp.Controllers
{
    public class HolesController : Controller
    {
        ILogger _logger;
        private readonly SurveyInstrumentAppContext _context;

        public HolesController(SurveyInstrumentAppContext context, ILogger<HolesController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: Holes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Holes.ToListAsync());
        }

        // GET: Holes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hole = await _context.Holes
                .Include(h => h.Readings)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);

            if (hole == null)
            {
                return NotFound();
            }
            
            //for each reading, get the calculate the previous 5 readings
            foreach (var currentReading in hole.Readings) {
                currentReading.Trustworthy = CalculateTrustworthy(currentReading, hole.Readings);
            }

            return View(hole);
        }

        private bool HoleExists(int id)
        {
            return _context.Holes.Any(e => e.ID == id);
        }

        private bool CalculateTrustworthy(Reading reading, ICollection<Reading> readings) {

            _logger.LogInformation("current Reading is  " + reading.ReadingID);

            //assume reading is trustworthy unless we state otherwise
            bool worthy = true;

            //filter collection to all previous readings relative to current reading
            var allPreviousReadings = from r in readings where r.ReadingID < reading.ReadingID select r;

            //take 5 previous readings from filtered collection (up to 5)
            IEnumerable<Reading> previousFiveReadings = allPreviousReadings.OrderByDescending(r => r.ReadingID).Take(5);
            //take 1 previous reading from filtered collection (up to 1)
            IEnumerable<Reading> previousReading = allPreviousReadings.OrderByDescending(r => r.ReadingID).Take(1);

            Boolean hasElements = previousFiveReadings.Any();  //maybe put this in a function
            if (hasElements)
            {
                //calculate average of previous readings
                var average = previousFiveReadings.Average(r => r.Dip);
                _logger.LogInformation("previousFiveReadings have an average of " + average);

                var previousAzimuth = previousReading.First().Azimuth;
                _logger.LogInformation("previousReading Azimuth is " + previousAzimuth);

                //check if current dip is within the threshold of the average of previous dips
                worthy = WithinDipThreshold(average, reading.Dip) && WithinAzimuthThreshold(previousAzimuth, reading.Azimuth);
            }

            return worthy;
        }

        private bool WithinDipThreshold(decimal DipA, decimal DipB) 
        {
            return Math.Abs(DipA - DipB) <= Constants.DipThreshold;
        }

        private bool WithinAzimuthThreshold(decimal AzimuthA, decimal AzimuthB)
        {
            return Math.Abs(AzimuthA - AzimuthB) <= Constants.AzimuthThreshold;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
