# Survey Instrument App Design
Brendan Downes - Nov 2018
## Introduction
This document describes the approach taken in order to develop the Survey Instruments web application.  It details the technical aspects of the solution, the design goals and assumptions that were made during the application’s development and summarizes the user journeys within the application.
## Functional Requirements
Geologists need a way to view several key pieces of information (readings) being generated from Survey Instruments used by mineral exploration clients.  These instruments measure the physical location of drilled holes as they track towards mineral bodies underground.

Specifically, these readings are:

- A single collar position (latitude, longitude, dip and azimuth) which is the surface position and orientation of the drill hole.
- A number of depth readings. Each reading has depth, dip and azimuth providing the direction of the hole at that point.

A web application has been developed for geologists to view this information.

#### User Journeys
The key user journeys for the application are:

1. Select an existing drill hole from a list.
2. View the collar position and depth readings for that drill hole.
3. Override any depth reading as being incorrect or not.

#### Business Rules
Each depth reading needs to be flagged as “trustworthy” or “not trustworthy” based on 2 business rules:

1. The azimuth is within 5 degrees of the previous depth’s azimuth reading.
2. The dip is within 3 degrees of the average dip from the previous 5 depth readings.

## Solution Design
In order to meet the requirements, several design goals have been taken into consideration.  The following section describes how these goals have been met.  Additionally, the technical design of the system is defined.

#### MVC
An ASP.NET MVC web application has been developed using C#.  Using an MVC (Model-View-Controller) approach allows for separation of concerns between the system’s components.  Each component is able to be developed, tested and maintained separately of eachother.

#### Model Hierarchy
The primary entity when viewing data from the Survey Instrument is the drill hole or “hole”.  Multiple unique drill holes exists, each with a number of depth readings, or “readings” associated with it.  These readings are unique to the hole itself.  As such a “one-to-many” relationship exists between the hole and its readings.

This is the foundation of the model design of the application:

- Model - Hole (one)
- Model - Reading (many)

#### Models
Two key models have been created.  **Hole** and **Reading**.

**Hole**
This is the drill hole entity.  The metadata associated with it is:
- ID (int) **PK** 		The unique ID of the hole.  This is also the **primary key**.
- Lat (decimal) 		The latitude associated as part of the drill hole “single collar position”.
- Lon (decimal)		The longitude associated as part of the drill hole “single collar position”.
- Dip (decimal)		The dip associated as part of the drill hole “single collar position”.
- Azimuth (decimal)	The azimuth associated as part of the drill hole “single collar position”.
- Readings (collection)	A reference to a collection of associated readings.

**Reading**
This is an individual drill hole reading.  The metadata associated with it is:
- ReadingID (int) **PK** 	The unique ID of the reading.  This is also the **primary key**.
- HoleID (int) **FK**		The ID of the related hole.  This is also a **foreign key to Hole**.
- Depth (decimal)		The depth associated with the direction of the hole.
- Dip (decimal)		The dip associated with the direction of the hole.
- Azimuth (decimal)	The azimuth associated with the direction of the hole.

#### Views
Three key views have been created.  **Holes/Index, Holes/Details** and **Readings/Edit**.

**Hole/Index**
- This is the default route for the application.
- This view presents a list of existing drill holes in a table layout.
- Each drill hole row is displayed with its “single collar position” metadata (readonly)
- The user can select a hole from this list to view readings in a separate view.

**Holes/Details**
- This view presents a list of readings for a specific drill hole in a table layout.
- The hole “single collar position” metadata is displayed (readonly)
- Each reading row is displayed with its “directional metadata” (readonly)
- Each reading row is displayed with a flag to indicate if the reading is “trustworthy”.
- Each reading row is displayed with a flag to indicate if the reading has been overridden as “incorrect”.
- The user can select a reading from this list to edit in a separate view.
- The user can select to navigate back to the drill hole list.

**Readings/Edit**
- This view presents a single reading to the user.
- The reading is displayed with its “directional metadata” (readonly)
- The user can check the “incorrect” flag.
- The user can click Save to initiate a POST operation.

#### Controllers
Two controllers have been created.  **HolesController** and **ReadingsController**.

**HolesController**
The following request mappings exist in the controller:
- Index (GET)
-- Uses the Entity Framework database context to obtain a list of Holes and return a View.
- Details (GET)
-- Uses route data to obtain the Hole “id”.
-- Uses the Entity Framework database context to return Hole model data and related Reading model data and return a View.
-- Calculates if each individual Reading is “trustworthy” by assessing relative previous Readings against a “Dip Threshold’ and “Azimuth Threshold”.

**ReadingsController**
The following request mappings exist in the controller:
- Edit (GET)
-- Uses route data to obtain the Reading “id”.
-- Uses the Entity Framework database context to return Reading model data and return a View.
- Edit (POST)
-- Uses route data to obtain the Reading “id” and also data binded from the form. 
-- Uses the Entity Framework database context to update the Reading.
-- Redirects to the Holes/Index controller.

#### Persistence
Entity Framework has been used along with the model classes to persist and manage data in a database.  It is the ORM (Object-relational mapping) framework used to abstract the data access logic to the database.

An embedded SQLite database is created along with its schema inside the SeedData.cs class based on the properties defined in the models.

Additionally, test data has been seeded into the database in the SeedData.cs class.

#### Business Logic
As mentioned in a previous section, each depth reading needs to be flagged as “trustworthy” or “not trustworthy” based on 2 business rules.  Here, we detail the approach taken to adhere to this logic.

“The azimuth is within 5 degrees of the previous depth’s azimuth reading” AND
“The dip is within 3 degrees of the average dip from the previous 5 depth readings”

- Relative to the current reading, previous readings need to be obtained. 	This is achieved by calling a LINQ function to obtain all readings where ReadingID is less that the current ReadingID.
- Once we have all the previous readings, we select the **previous 1 reading** and **previous 5 readings** and store in memory.
- We calculate the **average** of the **previous 5 readings** and assess this **average** value to be within a certain Dip threshold or not.
- We assess the **previous 1 reading** to be within a certain Azimuth threshold or not.
- We combine these two assessments to return the reading as “Trustworthy” or “Not Trustworthy”.

#### User Experience Design
- Responsive Design - As tables of data are being displayed, Bootstrap responsive tables have been utilized to allow for ease of horizontal scrolling on smaller devices.
- Bootstrap - This component library has been used to build a responsive, mobile first front-end.
- Layout View - A common Layout View has been utilized to provide a common layout to all pages.

#### Frameworks / Packages / Dependencies
ASP.NET Core 2.1.0 has been used to build this application.  As this is cross-platform this application was able to be developed on macOS.  The following NuGET packages are leveraged:
- Microsoft.AspNetCore.App (2.1.1)
- Microsoft.EntityFrameworkCore.Sqlite (2.1.4)

#### Logging
The default logging providers (Console, Debug and Event Source) are used in this application, via CreateDefaultBuilder extension method in the Program.cs.  This is generated from the default project template.

#### Form Validation
No input fields exist in the current application for user entry.  As such, form validation has not been a major part of this current design.

#### Security
- Authentication - Anonymous authentication is currently configured for the application.  No user/group/role based authentication is in place.
- XSRF - Anti-Request Forgery - An anti-forgery token is generated on the Readings/Edit View.  This is validated in the Readings/Edit(Post) method.
- Encryption - HTTPS Redirection Middleware is configured to redirect HTTP requests to HTTPS.

## Assumptions
- “previous depth’s azimuth reading”  - This is assumed to be the previous reading ID for the drill hole, not the nearest depth above the current depth.
- Dip - We have assumed for this application that this is measured as a decimal value between 0-90 (degrees).
- The first reading of each drill hole is assumed to be trustworthy, as it does not have any “previous” readings relative to it.
- If a reading does not have “5 previous depth readings”, an average is taken of the previous readings it does have.  This may be 1,2,3 or 4 readings.  The trustworthy calculation is still made on these previous readings.

## Future Enhancements
The following enhancements could be implemented:
- Exception handling for malformed URLs.
- Hosting - application could be hosted in Azure or other cloud platform.  Database provider could be reconsidered.
- Paging / Sorting / Filtering / Search - Currently there is no paging occuring on the Readings list.  This is not scalable.  Paging (or lazy loading) and subsequent sorting, filtering and search to aid in navigation should be implemented.
- In-memory performance - The calculation of the average dip is being performed by iterating through the list of Readings for each Reading and storing these in memory within the controller.  This has a impact of space and time complexity and should be improved upon.
- Front-End Frameworks - Angular or React could be implemented to enhance front end or to include SPA (Single Page Application) functionality. 
- Network traffic is not encrypted in the load of POST calls to the Readings/Edit controller.  This could be encrypted to enhance security.
- Navigation - There is currently no navigation from the Readings/Edit view to the Holes/Details view.  This should be added to stop user having to go back to the root page.
- Testing - No unit tests are currently written for the application.  However controller unit tests should be written to test the individual action methods, filters, routing and model binding.  Additionally utility methods for determining “trustworthy” status can be unit tested in isolation due to us having separate methods for these.
- Trustworthy flag is calculated in memory within a controller.  This is then not displayed in the Readings/Edit view nor is it persisted to the database.  Perhaps an improvement to this design should be considered.