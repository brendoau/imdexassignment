using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SurveyInstrumentApp.Models;

namespace SurveyInstrumentApp.Controllers
{
    public class ReadingsController : Controller
    {
        ILogger _logger;
        private readonly SurveyInstrumentAppContext _context;

        public ReadingsController(SurveyInstrumentAppContext context, ILogger<ReadingsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: Readings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reading = await _context.Readings.FindAsync(id);
            if (reading == null)
            {
                return NotFound();
            }
            return View(reading);
        }

        // POST: Readings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ReadingID,HoleID,Depth,Dip,Azimuth,Incorrect")] Reading reading)
        {
            if (id != reading.ReadingID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reading);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReadingExists(reading.ReadingID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index","Holes");
            }
            return View(reading);
        }

        private bool ReadingExists(int id)
        {
            return _context.Readings.Any(e => e.ReadingID == id);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
