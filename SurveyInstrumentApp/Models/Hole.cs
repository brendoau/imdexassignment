﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SurveyInstrumentApp.Models
{
    public class Hole
    {

        public int ID { get; set; }
        [Display(Name = "Latitude")]         public decimal Lat { get; set; }
        [Display(Name = "Longitude")]         public decimal Lon { get; set; }         public decimal Dip { get; set; }         public decimal Azimuth { get; set; } 
        public ICollection<Reading> Readings { get; set; }
    }
}
